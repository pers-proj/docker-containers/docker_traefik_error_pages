# pull official base image
FROM alpine:latest AS base
RUN apk --update upgrade --no-cache

# use updated official image as new image base
FROM base AS release

# set work directory
WORKDIR /error_pages

# copy entrypoint
COPY --chown=1000:1000 --chmod=0774 entrypoint.sh /

# set environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    FLASK_APP=run.py \
    FLASK_ENV=production \
    FLASK_DEBUG=False \
    FLASK_USER=flask \
    APP_PORT=5000 \
    VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install dependencies
RUN apk add --update --no-cache python3 curl && \
    python3 -m venv $VIRTUAL_ENV --upgrade-deps && \
    addgroup $FLASK_USER -g 1000 && \
    adduser $FLASK_USER --no-create-home --disabled-password --ingroup $FLASK_USER --uid 1000
COPY --chown=1000:1000 ./app/ .
RUN pip3 install -r pip-requirements.txt

# change user
USER $FLASK_USER:$FLASK_USER

# configure default healthcheck
# Note: The redirections of stderr and stdout are in this order
# deliberately. This way "default" stdout gets redirected to
# /dev/null while stderr outputs to stdout (and does not get
# redirected to /dev/null)
HEALTHCHECK --interval=60s --start-period=10s --retries=5 \
    CMD /usr/bin/curl -fsS localhost:$APP_PORT/200.html 2>&1 >/dev/null

# run gunicorn
ENTRYPOINT ["/entrypoint.sh"]

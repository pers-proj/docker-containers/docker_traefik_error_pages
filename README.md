# What is this? #

This is a simple flask container hosting custom error pages. It is based on the latest alpine image to be as small as possible.

This container is intended to be used using Traefik to serve the same error pages for all services. It will also register a _catchall_ router with lowest priority of `1` so that any Traefik 404 error also gets caught by this service. This allows for beautiful 404 pages instead of a simple `404 page not found` displayed by Traefik.

The error page design has been written by [_Saleh Riaz_](http://salehriaz.com) and may be found in its original on [their website](http://salehriaz.com/404Page/404.html).

_**Note**: This image has been designed with my personal needs in mind!_

# How to #

When running this image, it is important to configure Traefik accordingly. The `docker-compose.yml` on the Bitbucket repository shows a working configuration with the following prerequisites:
- A docker network with name "proxy" for internal container exchange (both, Traefik and this container are in this network - **optional**)
- A Traefik middleware named "security" and defined in file (this sets some security headers - **optional**)
- An entrypoint named "web-secure" in Traefik (may be named anything you want)

This configuration assumes an independent redirection of any HTTP call to HTTPS. If this is not the case in your setup, you will have to register another catch-all rule.

# Security #

This container will by default run as non-root user `flask`. As a consequence, you may not use ports below `1024` for `$APP_PORT` (default is `5000`)
